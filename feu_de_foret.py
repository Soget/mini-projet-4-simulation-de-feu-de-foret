from tkinter import *
from random import *

vide = 0
feu = 1
cendre = 2
arbre = 3

choix_proba = 60
haut = 20
larg = 20
cote = 15
nombre_de_feu = 0

cell = [[0 for row in range(haut)] for col in range(larg)]
etat = [[vide for row in range(haut)] for col in range(larg)]
temp = [[vide for row in range(haut)] for col in range(larg)]


# Calculer et dessiner le prochain tableau
def tableau():
    calculer(nombre_de_feu)
    dessiner()
    fenetre.after(300, tableau)
def go(nombre_de_feu):
    while(nombre_de_feu > 0):
        print("PUTE")
        tableau()

def init():
    canvas.delete("all")
    for y in range(haut):
        for x in range(larg):
            cell[x][y] = canvas.create_rectangle((x*cote, y*cote, (x+1)*cote, (y+1)*cote), outline="gray", fill="white")
    for y in range(haut):
        for x in range(larg):
            proba = randint(0, 100)
            if proba < choix_proba:
                etat[x][y] = arbre
            else:
                etat[x][y] = vide


def click_callback(event):
    """Dessine un foyer de feu la ou l'utilisateur a cliqué"""
    nombre_de_feu = 0
    x1=event.x%15
    x1=(event.x-x1)+8

    y1=event.y%15
    y1=(event.y-y1)+8

    colonne_click = x1 // cote
    ligne_click = y1 // cote
    
    if etat[ligne_click][colonne_click] == arbre:
        feu = canvas.create_oval(x1-5, y1-5, x1+5, y1+5,fill='Red') # 5 correspond au rayon de notre cercle 
        etat[ligne_click][colonne_click] = feu
        nombre_de_feu = nombre_de_feu + 1
        return nombre_de_feu


def calculer(nombre_de_feu):
    for y in range(haut):
        for x in range(larg):
            voisins_en_feu = compte_feu(x,y)

            if voisins_en_feu == TRUE and etat[x][y] != cendre or etat[x][y] != vide:
                temp[x][y] = feu
                nombre_de_feu = nombre_de_feu + 1
            if etat[x][y] == feu:
                temp[x][y] = cendre
                nombre_de_feu -= 1
    for y in range(haut):
        for x in range(larg):
            etat[x][y] = temp[x][y]

def compte_feu(x,y):
    voisins_en_feu = FALSE
    if etat[(x-1)%larg][(y)%haut] == feu:
         voisins_en_feu = TRUE
    if etat[(x+1)%larg][(y)%haut] == feu:
        voisins_en_feu = TRUE
    if etat[(x)%larg][(y-1)%haut] == feu:
        voisins_en_feu = TRUE
    if etat[(x)%larg][(y+1)%haut] == feu:
        voisins_en_feu = TRUE
    return voisins_en_feu

# Dessiner toutes les cellules
def dessiner():
    for y in range(haut):
        for x in range(larg):
            if etat[x][y] == vide:
                couleur = "White"
            if etat[x][y] == feu:
                couleur = "Red"
            if etat[x][y] == cendre:
                couleur = "Grey"
            if etat[x][y] == arbre:
                couleur = "Green"
            canvas.itemconfig(cell[x][y], fill=couleur)

def reset():
    init()
    dessiner()

# Creation Fenetre
fenetre = Tk()
fenetre.title("Simulation de feux de forêt")
canvas = Canvas(fenetre, bg='grey', width=cote*larg, height=cote*haut, highlightthickness=0)
canvas.pack(side = TOP, padx = 5, pady = 5)
canvas.bind("<Button-1>", click_callback)

# Interface du programme
reset_button = Button(fenetre, text ='Reset', command = reset)
reset_button.pack(side =LEFT, padx =3, pady =3)

quit_button = Button(fenetre, text ='Quitter', command= fenetre.destroy)
quit_button.pack(side =RIGHT,padx =3, pady =3)

# Lancement du programme
init()
dessiner()
go()
fenetre.mainloop()