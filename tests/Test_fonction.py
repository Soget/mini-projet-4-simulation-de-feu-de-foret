import unittest
from feu_de_foret import *

class Test_fonction(unittest.TestCase):

    def test_click_callback(self):
        self.assertEqual(click_callback(case1.fill == 'green'), case1.fill == 'red')
        self.assertEqual(click_callback(case2.fill == 'white'), case2.fill == 'white')

    def test_calculer(self):
        case1.fill == 'green'
        case2.fill == 'red'
        case3.fill == 'white'

        self.assertEqual(calculer() case1.fill == 'red')
        self.assertEqual(calculer() case2.fill == 'grey')
        self.assertEqual(calculer() case3.fill == 'white')